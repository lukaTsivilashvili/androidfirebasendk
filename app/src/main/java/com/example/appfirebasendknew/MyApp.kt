package com.example.appfirebasendknew

import android.app.Application
import android.content.Context

class MyApp : Application() {

    override fun onCreate() {
        super.onCreate()
        key = apiKey()
        context = applicationContext
    }

    companion object {

        lateinit var context:Context

        lateinit var key:String

        init {
            System.loadLibrary("native-lib")
        }
    }


    external fun apiKey(): String


}