package com.example.appfirebasendknew.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.example.appfirebasendknew.viewmodels.AuthViewModel
import com.example.appfirebasendknew.databinding.AuthFragmentBinding
import com.example.appfirebasendknew.models.UserModel

class AuthFragment : Fragment() {

    private val viewModel: AuthViewModel by viewModels()
    private lateinit var binding: AuthFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = AuthFragmentBinding.inflate(inflater, container, false)

        init()

        return binding.root
    }


    private fun init() {

        binding.regButton.setOnClickListener {

            viewModel.initSignUp()
            observes()
            Toast.makeText(requireActivity(), viewModel.userInfo.email, Toast.LENGTH_SHORT).show()

        }

        binding.signButton.setOnClickListener {
            viewModel.initSignIn()
            observes()
        }

    }


    private fun observes() {

        viewModel.userInfo =
            UserModel(binding.ETemail.text.toString(), binding.ETpassword.text.toString())

    }

}