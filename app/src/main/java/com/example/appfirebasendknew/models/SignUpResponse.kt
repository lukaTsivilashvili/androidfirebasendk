package com.example.appfirebasendknew.models

data class SignUpResponse(
    val idToken: String?,
    val email: String?,
    val refreshToken: String?,
    val expiresIn: String?,
    val localId: String?
)
