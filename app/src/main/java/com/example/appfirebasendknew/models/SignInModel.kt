package com.example.appfirebasendknew.models

data class SignInModel(
    val idToken: String?,
    val email: String?,
    val refreshToken: String?,
    val expiresIn: String?,
    val localId: String?,
    val registered:Boolean = false
)
