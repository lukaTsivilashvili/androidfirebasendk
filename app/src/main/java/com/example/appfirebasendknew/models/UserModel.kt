package com.example.appfirebasendknew.models

data class UserModel
    (
    val email: String?,
    val password: String?,
    val returnSecureToken:Boolean = true
)
