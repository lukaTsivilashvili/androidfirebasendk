package com.example.appfirebasendknew.viewmodels

import android.util.Log.d
import androidx.lifecycle.ViewModel
import com.example.appfirebasendknew.models.UserModel
import com.example.appfirebasendknew.retrofitStuff.RetrofitInstance
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AuthViewModel : ViewModel() {


    lateinit var userInfo:UserModel


    private suspend fun signUp() {
        val result = RetrofitInstance.service().signUp(userInfo)
        d("myLog", result.body().toString())
    }

    private suspend fun signIn() {
        val result = RetrofitInstance.service().signIn(userInfo)
        d("myLog", result.body().toString())
    }


    fun initSignUp() {
        CoroutineScope(Dispatchers.IO).launch {
            signUp()
        }
    }

    fun initSignIn(){
        CoroutineScope(Dispatchers.IO).launch {
            signIn()
        }
    }


}