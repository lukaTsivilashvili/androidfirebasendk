package com.example.appfirebasendknew.retrofitStuff

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitInstance {

    fun service(): ApiInterface {
        return Retrofit.Builder()
            .baseUrl("https://identitytoolkit.googleapis.com")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiInterface::class.java)

    }

}