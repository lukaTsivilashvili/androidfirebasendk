package com.example.appfirebasendknew.retrofitStuff

import com.example.appfirebasendknew.models.UserModel
import com.example.appfirebasendknew.MyApp
import com.example.appfirebasendknew.models.SignInModel
import com.example.appfirebasendknew.models.SignUpResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

interface ApiInterface {

    @POST("/v1/accounts:signUp")

    suspend fun signUp(
        @Body user: UserModel, @Query("key")
        apiKey: String = MyApp.key
    ): Response<SignUpResponse>


    @POST("/v1/accounts:signInWithPassword")
    suspend fun signIn(
        @Body user: UserModel, @Query("key")
        apiKey: String = MyApp.key
    ): Response<SignInModel>
}